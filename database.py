import pymongo
from datetime import datetime
import os

MONGODB_URI = os.environ.get('MongoDB')
DATABASE_NAME = "IPT8.1_Heizung"
COLLECTION_NAME = "temperatures"  

def insert_temperature(temperature):
    client = pymongo.MongoClient(MONGODB_URI)
    db = client[DATABASE_NAME]
    collection = db[COLLECTION_NAME]
    

    temperature_doc = {
        "Soll-Temperatur": solltemperature,
        "Ist-Temperatur": isttemperature
        "timestamp": datetime.now()
    }
    

    collection.insert_one(temperature_doc)
    

    client.close()


temperature_data = 25.5


insert_temperature(temperature_data)

print("Temperature data inserted into MongoDB successfully.")
